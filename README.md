## The Kitchen

This is where behind-the-scenes work and discussions happen for the labs.

## Definition  of The New Aurora Labs

Objective: Delivers AI experiences through exploratory experimentation, data-driven learning, and forward-thinking efficiency.

1. **Experimentation**:

    - Get our hands dirty with AI and explore exciting ideas or potential.
    - Transform these ideas into tangible, working projects.
    - Leverage a fast-paced cycle of building, breaking, tweaking, and repeating.

2. **Learning**:

    - Capture valuable data and insights from every project, regardless of its success.
    - Identify the impact to determine what works, what doesn't, and what generates the most interest.

3. **Efficiency**:

    - Deliver quickly.
    - Embrace the ephemeral, its learnings, and the excitement of what comes next.
    - Don't wait.

## Our principles

1. **Openness**

   - AI is a transformational technology in the world. We want to be on the side of the table that drives this technology with openness, transparency and accountability, empowering society.
   - Empowering society means improving equality.

## Current Areas

- Online learning & tutoring.
- Financial advising.
